all: index.html

# this is only going to keep working properly as long as I have fewer chapters
# than the max number of arguments to xargs
index.html: entries/* render.pl footer.html header.html
	ls entries/* | xargs ./render.pl | cat header.html - footer.html > $@

publish: index.html
	git push
	ssh -A squiggle.city 'cd public_html/workings-book && git pull && git feed > feed.xml'
