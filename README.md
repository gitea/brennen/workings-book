workings:  a technical notebook
===============================

This is the source for some text which can be found here:

[http://squiggle.city/~brennen/workings-book/](//squiggle.city/~brennen/workings-book/)

...which is probably the best place to read it.

what
----

I attempt to keep a notebook about the technical problems I encounter and solve
(or attempt to solve), the research I conduct along the way, and the patterns
or general principles I try to extract from my work.

how
---

This is mostly just some Markdown files and a handful of scripts.  Chapters are
listed in a file called `chapters`; each chapter is a directory containing an
`index.md` and any supplemental files.  These are pushed through `render.pl`
and concatenated with `header.html` & `footer.html`.  For convenience, this is
handled by a basic `Makefile`:

    $ make
    cat chapters | xargs ./render.pl | cat header.html - footer.html > index.html

`render.pl` wraps up a library called `Text::Markdown::Discount`, which in turn
wraps up the Discount Markdown parser.  Aside from this, it also generates a
table of contents and expands code of the form

    <!-- exec -->

        $ [some command]

    <!-- end -->

to include the output of the command executed in the directory containing the
rendered file, which is how most of the example commands in the text are
rendered.  In keeping with long tradition, this is done with cheeseball regex
substitution.  **Be aware that, by running make, you are implicitly trusting
most of the commands found in the text to run on your system.**  This is no
weirder than the level of trust extended to most any build process, but I don't
want it to catch anyone by surprise.

There's also a `links.md`, which should contain all links to external
resources.

To build the whole thing without errors, you'd need to be on a system with a
couple of external repos, Bash and the GNU coreutils, and miscellaneous
utilities (notably `cowsay`, `dict`, `figlet`, `aspell`, `curl`, `lynx`, and
`w3m`).  A recent Debian or some derivative would probably work best.  One of
these days I'll wrap up all the dependencies in a package.

author
------

Brennen Bearnes (p1k3.com / @brennen)
