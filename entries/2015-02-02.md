Monday, February  2
===================

kernel-o-matic & pi finder
--------------------------

Published Friday:

- https://github.com/adafruit/Adafruit-Pi-Kernel-o-Matic
- https://learn.adafruit.com/raspberry-pi-kernel-o-matic

Published a week or so before:

- https://github.com/adafruit/Adafruit-Pi-Finder
- https://blog.adafruit.com/2015/01/23/help-us-test-our-new-raspberry-pi-bootstrap-piday-raspberrypi/

These have been a lot of my working time these last couple weeks, an
overlapping set of projects aimed at making the Pi (and eventually other
single-board computers) more usable and Adafruit's hardware and tutorials both
more accessible.  This has been frustrating and rewarding by turns.  I'm trying
to reduce the complexity of a domain I just barely understand, in a lot of
ways, which may be a good summary of software development in general.

[Vagrant](https://www.vagrantup.com/) is something I should have paid attention
to sooner.  The interfaces to virtualization are finally starting to overcome
my innate laziness on the whole question.

I just booted a Windows XP box for some reason.  It made that noise.  You know
the one.

raspberry pi 2
--------------

Announced today:

- http://www.raspberrypi.org/products/raspberry-pi-2-model-b/
- https://blog.adafruit.com/2015/02/02/raspberry-pi-2-model-b-armv7-with-1g-ram-is-here-benchmarks-and-more-raspberry_pi-raspberryp/
- http://hackaday.com/2015/02/02/introducing-the-raspberry-pi-2/

Expect this to prove interesting.  I've been having a lot of conversations
about the relative merits of small computing systems, and while this can hardly
be said to address all the complaints you might have about the Pi, boosting
processor and RAM will do a lot for practical usability.

telling composer to ignore php version requirements
---------------------------------------------------

Using [Composer](https://getcomposer.org/) to set up a little project, I run
into the problem that the locally-installed PHP is a bit behind the times.

    brennen@exuberance 0:09:32 /home/brennen/code/project $  ./composer.phar install
    Loading composer repositories with package information
    Installing dependencies (including require-dev)
    Your requirements could not be resolved to an installable set of packages.

      Problem 1
        - sparkfun/sparklib 1.1.9 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.8 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.7 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.6 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.5 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.4 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.3 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.2 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.11 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.10 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.1 requires php >=5.5.17 -> no matching package found.
        - sparkfun/sparklib 1.1.0 requires php >=5.5.17 -> no matching package found.
        - Installation request for sparkfun/sparklib ~1.1 -> satisfiable by sparkfun/sparklib[1.1.0, 1.1.1, 1.1.10, 1.1.11, 1.1.2, 1.1.3, 1.1.4, 1.1.5, 1.1.6, 1.1.7, 1.1.8, 1.1.9].

    Potential causes:
     - A typo in the package name
     - The package is not available in a stable-enough version according to your minimum-stability setting
       see <https://groups.google.com/d/topic/composer-dev/_g3ASeIFlrc/discussion> for more details.

    Read <http://getcomposer.org/doc/articles/troubleshooting.md> for further common problems.

Well, ok.  I wrote a lot of that code, and I'm pretty sure nothing I want out
of it will break under a slightly stale PHP.  I check `./composer.phar help install`,
and sure enough, there's an option to ignore this requirement:

    brennen@exuberance 0:13:21 /home/brennen/code/project $  ./composer.phar install --ignore-platform-reqs  
    Loading composer repositories with package information
    Installing dependencies (including require-dev)
      - Installing sparkfun/sparklib (1.1.11)
        Downloading: 100%         

    Writing lock file
    Generating autoload files

I never used to quite get the "install an executable utility script in the root
directory of your project" thing, but the whole paradigm is growing on me a little
as my projects accumulate little Makefiles and shell scripts to render HTML,
publish revisions, or deploy packages.
