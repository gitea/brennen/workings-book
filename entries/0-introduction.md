a technical notebook
====================

I've been working on [a short book][userland-book] on the basics of the command
line.  This one is something else altogether.  It's a long-form technical
logbook or journal, capturing the outlines of problems and their solutions as I
encounter them.  It is dry, incomplete, and sometimes wrong.  It is unlikely to
be useful for the general reader.  I'm compiling it because I want a record of
what I learn, and I hope that writing documentation will help me do cleaner,
more reproducible work.  If it helps people with similar problems along the
way, so much the better.

--- bpb / [p1k3](https://p1k3.com)
/ [@brennen](https://ello.co/brennen)
/ [~brennen](http://squiggle.city/~brennen/)

copying
-------

[CC BY-SA 4.0][cc-by-sa]

<div class=details>
  <h2 class=clicker>contents</h2>
  <div class=full>
    {{contents}}
  </div>
</div>
