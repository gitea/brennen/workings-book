Wednesday, December 10, 2014
============================

listusers / squiggle.city repo
------------------------------

There's now a [squigglecity organization on GitHub][squiggle.city.gitorg].
What little is there is a classic duct-tape mess complete with a bunch of
commits made as root, but may contain a few useful bits.

I'm planning to clean up [this version of listusers.pl][listusers-dec-10] into a
more generic `listusers` utility that just outputs TSV and pipe to csvkit / `jq`
for HTML & JSON.

Oh, right --- about the JSON.  ~ford proposed a standard `tilde.json` [kind of
like this](http://squiggle.city/tilde.json), which I think is not a terrible
idea at all though that one's a bit rough and the format could still use a
little tweaking as of this writing.

This is the kind of thing it's unbelievably easy to overthink.  I'm hoping
we'll give it enough thought to do a few smart things but not so much thought
that no one actually uses it.

[listusers-dec-10]: https://github.com/squigglecity/squiggle.city/blob/1a07ccc8415b05ad239116a062d2992ae1537541/listusers.pl
