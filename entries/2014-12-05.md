Friday, December 5, 2014
========================

notes on vim
------------

Vim is a text editor.  My slowly-evolving configuration can be found on GitHub,
in [bpb-kit][bpb-kit].

[Tyler Cipriani][thcipriani] is a lot smarter than I am about vim (and, in
fact, most things), but I am particular and don't always share his preferences.

keybindings
-----------

I'm starting in on this notebook, which uses a Makefile, and think it might be
nice to have a quick vim keybinding for `:make`.  I would use `F5`, by analogy
to QBasic, but I've already bound that to `:wall`, which writes all the open
buffers with changes.

I think that maybe `<leader>m`, which in my case means `,m`, would be ok.  Then
I'm not sure if something is already mapped starting with that, so I try `:map`.

I want to search through the list produced by `:map`, and think it'd be nice if
I could just read it into a buffer.  The first thing I google is "vim read
output of command into file".  This could easily enough give hits for reading
the output of a shell command, but the 3rd thing down the page is
[Capture ex command output](http://vim.wikia.com/wiki/Capture_ex_command_output)
on the Vim Tips Wiki.

There are a bunch of interesting ideas there, but the first basic idea is this:

    :redir @a
    :set all
    :redir END

Then you can open a new buffer - `:new` - and do `"ap`.  This says "using the named
register a, paste".

This seems to work with `:set all`, but not so much with `:map`.  Why not?  I skim
`:help map` and `help redir` without getting very far.  Updates to come.

With that digression still unanswered, the mapping I settled on is simple:

    nmap <leader>m :make<CR>

I never know if these are going to take with me.  The handful of custom
bindings that have actually entered my vocabulary are super-useful, but more
often than not I wind up forgetting about an idea not long after I've
implemented it.
