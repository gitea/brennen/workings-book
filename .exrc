if &cp | set nocp | endif
let s:cpo_save=&cpo
set cpo&vim
noremap! <M-BS> 
noremap! <expr> <SNR>100_transpose "\<BS>\<Right>".matchstr(getcmdline()[0 : getcmdpos()-2], '.$')
noremap! <expr> <SNR>100_transposition getcmdpos()>strlen(getcmdline())?"\<Left>":getcmdpos()>1?'':"\<Right>"
imap <C-Tab> :tabnexti
imap <C-S-Tab> :tabpreviousi
imap <F5> :wall
imap <F4> :set invnumbera
map! <S-Insert> <MiddleMouse>
map <NL> j_
map  k_
nnoremap <silent>  :CtrlP
nmap <silent> ,slr :DBListVar
xmap <silent> ,sa :DBVarRangeAssign
nmap <silent> ,sap :'<,'>DBVarRangeAssign
nmap <silent> ,sal :.,.DBVarRangeAssign
nmap <silent> ,sas :1,$DBVarRangeAssign
nmap ,so <Plug>DBOrientationToggle
nmap ,sh <Plug>DBHistory
xmap <silent> ,stcl :exec "DBListColumn '".DB_getVisualBlock()."'"
nmap ,stcl <Plug>DBListColumn
nmap ,slv <Plug>DBListView
nmap ,slp <Plug>DBListProcedure
nmap ,slt <Plug>DBListTable
xmap <silent> ,slc :exec "DBListColumn '".DB_getVisualBlock()."'"
nmap ,slc <Plug>DBListColumn
nmap ,sbp <Plug>DBPromptForBufferParameters
nmap ,sdpa <Plug>DBDescribeProcedureAskName
xmap <silent> ,sdp :exec "DBDescribeProcedure '".DB_getVisualBlock()."'"
nmap ,sdp <Plug>DBDescribeProcedure
nmap ,sdta <Plug>DBDescribeTableAskName
xmap <silent> ,sdt :exec "DBDescribeTable '".DB_getVisualBlock()."'"
nmap ,sdt <Plug>DBDescribeTable
xmap <silent> ,sT :exec "DBSelectFromTableTopX '".DB_getVisualBlock()."'"
nmap ,sT <Plug>DBSelectFromTopXTable
nmap ,sta <Plug>DBSelectFromTableAskName
nmap ,stw <Plug>DBSelectFromTableWithWhere
xmap <silent> ,st :exec "DBSelectFromTable '".DB_getVisualBlock()."'"
nmap ,st <Plug>DBSelectFromTable
nmap <silent> ,sep :'<,'>DBExecRangeSQL
nmap <silent> ,sel :.,.DBExecRangeSQL
nmap <silent> ,sea :1,$DBExecRangeSQL
nmap ,sq <Plug>DBExecSQL
nmap ,sE <Plug>DBExecSQLUnderTopXCursor
nmap ,se <Plug>DBExecSQLUnderCursor
xmap ,sE <Plug>DBExecVisualTopXSQL
xmap ,se <Plug>DBExecVisualSQL
map ,ds <Plug>DrawItStop
map ,di <Plug>DrawItStart
nmap <silent> ,cv <Plug>VCSVimDiff
nmap <silent> ,cu <Plug>VCSUpdate
nmap <silent> ,cU <Plug>VCSUnlock
nmap <silent> ,cs <Plug>VCSStatus
nmap <silent> ,cr <Plug>VCSReview
nmap <silent> ,cq <Plug>VCSRevert
nmap <silent> ,cn <Plug>VCSAnnotate
nmap <silent> ,cN <Plug>VCSSplitAnnotate
nmap <silent> ,cl <Plug>VCSLog
nmap <silent> ,cL <Plug>VCSLock
nmap <silent> ,ci <Plug>VCSInfo
nmap <silent> ,cg <Plug>VCSGotoOriginal
nmap <silent> ,cG <Plug>VCSClearAndGotoOriginal
nmap <silent> ,cd <Plug>VCSDiff
nmap <silent> ,cD <Plug>VCSDelete
nmap <silent> ,cc <Plug>VCSCommit
nmap <silent> ,ca <Plug>VCSAdd
map ,rwp <Plug>RestoreWinPosn
map ,swp <Plug>SaveWinPosn
map ,tt <Plug>AM_tt
map ,tsq <Plug>AM_tsq
map ,tsp <Plug>AM_tsp
map ,tml <Plug>AM_tml
map ,tab <Plug>AM_tab
map ,m= <Plug>AM_m=
map ,tW@ <Plug>AM_tW@
map ,t@ <Plug>AM_t@
map ,t~ <Plug>AM_t~
map ,t? <Plug>AM_t?
map ,w= <Plug>AM_w=
map ,ts= <Plug>AM_ts=
map ,ts< <Plug>AM_ts<
map ,ts; <Plug>AM_ts;
map ,ts: <Plug>AM_ts:
map ,ts, <Plug>AM_ts,
map ,t= <Plug>AM_t=
map ,t< <Plug>AM_t<
map ,t; <Plug>AM_t;
map ,t: <Plug>AM_t:
map ,t, <Plug>AM_t,
map ,t# <Plug>AM_t#
map ,t| <Plug>AM_t|
map ,T~ <Plug>AM_T~
map ,Tsp <Plug>AM_Tsp
map ,Tab <Plug>AM_Tab
map ,TW@ <Plug>AM_TW@
map ,T@ <Plug>AM_T@
map ,T? <Plug>AM_T?
map ,T= <Plug>AM_T=
map ,T< <Plug>AM_T<
map ,T; <Plug>AM_T;
map ,T: <Plug>AM_T:
map ,Ts, <Plug>AM_Ts,
map ,T, <Plug>AM_T,o
map ,T# <Plug>AM_T#
map ,T| <Plug>AM_T|
map ,Htd <Plug>AM_Htd
map ,anum <Plug>AM_aunum
map ,aenum <Plug>AM_aenum
map ,aunum <Plug>AM_aunum
map ,afnc <Plug>AM_afnc
map ,adef <Plug>AM_adef
map ,adec <Plug>AM_adec
map ,ascom <Plug>AM_ascom
map ,aocom <Plug>AM_aocom
map ,adcom <Plug>AM_adcom
map ,acom <Plug>AM_acom
map ,abox <Plug>AM_abox
map ,a( <Plug>AM_a(
map ,a= <Plug>AM_a=
map ,a< <Plug>AM_a<
map ,a, <Plug>AM_a,
map ,a? <Plug>AM_a?
nmap <silent> ,s :set nolist!:redr
noremap ,c :SyntasticCheck
nmap ,ok :!addprop % meta-ok-poem:e
nmap ,f :FufFile
nmap ,tn :tabnew
nmap ,l :Glog:copen
nmap ,w :wall
nmap ,p :cprev
nmap ,n :cnext
nmap ,q gqip
nmap ,d :call BPB_DecorateFilter()
nmap ,r :call BPB_ExecFilter()
nmap ,td :.-1r !today
noremap! ð <Up>
noremap! î <Down>
noremap! æ <S-Right>
cnoremap ä <S-Right>
inoremap ä dw
noremap! â <S-Left>
map K ig;
xmap S <Plug>VSurround
vmap [% [%m'gv``
vmap ]% ]%m'gv``
vmap a% [%v]%
nmap cs <Plug>Csurround
nmap ds <Plug>Dsurround
nmap gx <Plug>NetrwBrowseX
xmap gS <Plug>VgSurround
nmap ySS <Plug>YSsurround
nmap ySs <Plug>YSsurround
nmap yss <Plug>Yssurround
nmap yS <Plug>YSurround
nmap ys <Plug>Ysurround
nnoremap <silent> <Plug>NetrwBrowseX :call netrw#NetrwBrowseX(expand("<cWORD>"),0)
noremap <silent> <Plug>DrawItStop :set lz:call DrawIt#DrawItStop():set nolz
noremap <silent> <Plug>DrawItStart :set lz:call DrawIt#DrawItStart():set nolz
nnoremap <silent> <Plug>SurroundRepeat .
nnoremap <silent> <Plug>VCSVimDiff :VCSVimDiff
nnoremap <silent> <Plug>VCSUpdate :VCSUpdate
nnoremap <silent> <Plug>VCSUnlock :VCSUnlock
nnoremap <silent> <Plug>VCSStatus :VCSStatus
nnoremap <silent> <Plug>VCSSplitAnnotate :VCSAnnotate!
nnoremap <silent> <Plug>VCSReview :VCSReview
nnoremap <silent> <Plug>VCSRevert :VCSRevert
nnoremap <silent> <Plug>VCSLog :VCSLog
nnoremap <silent> <Plug>VCSLock :VCSLock
nnoremap <silent> <Plug>VCSInfo :VCSInfo
nnoremap <silent> <Plug>VCSClearAndGotoOriginal :VCSGotoOriginal!
nnoremap <silent> <Plug>VCSGotoOriginal :VCSGotoOriginal
nnoremap <silent> <Plug>VCSDiff :VCSDiff
nnoremap <silent> <Plug>VCSDelete :VCSDelete
nnoremap <silent> <Plug>VCSCommit :VCSCommit
nnoremap <silent> <Plug>VCSAnnotate :VCSAnnotate
nnoremap <silent> <Plug>VCSAdd :VCSAdd
nmap <silent> <Plug>RestoreWinPosn :call RestoreWinPosn()
nmap <silent> <Plug>SaveWinPosn :call SaveWinPosn()
nmap <SNR>50_WE <Plug>AlignMapsWrapperEnd
map <SNR>50_WS <Plug>AlignMapsWrapperStart
map <M-Left> h
map <M-Right> l
map <C-Tab> :tabnext
map <C-S-Tab> :tabprevious
map <F9> :set invhlsearch
map <F5> :wall
map <F4> :set invnumber
map <F3> :NERDTreeFind
map <F2> :NERDTreeToggle
map <S-Insert> <MiddleMouse>
cnoremap  <Home>
inoremap  ^
cnoremap  <Left>
inoremap <expr>  getline('.')=~'^\s*$'&&col('.')>strlen(getline('.'))?"0\<C-D>\<Esc>kJs":"\<Left>"
cnoremap <expr>  getcmdpos()>strlen(getcmdline())?"\<C-D>":"\<Del>"
inoremap <expr>  col('.')>strlen(getline('.'))?"\<C-D>":"\<Del>"
inoremap <expr>  col('.')>strlen(getline('.'))?"\<C-E>":"\<End>"
cnoremap <expr>  getcmdpos()>strlen(getcmdline())?&cedit:"\<Right>"
inoremap <expr>  col('.')>strlen(getline('.'))?"\<C-F>":"\<Right>"
imap S <Plug>ISurround
imap s <Plug>Isurround
imap  <Plug>Isurround
cnoremap  
inoremap  
let &cpo=s:cpo_save
unlet s:cpo_save
set autoindent
set autoread
set background=dark
set backspace=indent,eol,start
set directory=~/.vim/swapfiles,.,/tmp
set expandtab
set fileencodings=ucs-bom,utf-8,default,latin1
set foldopen=block,hor,mark,percent,quickfix,tag,undo
set guioptions=aegimrLt
set helplang=en
set history=3500
set ignorecase
set incsearch
set isfname=@,48-57,/,.,-,_,+,,,#,$,%,~,=,:
set iskeyword=@,48-57,_,192-255,:
set listchars=tab:⇾\ ,trail:·
set mouse=a
set printoptions=paper:letter
set ruler
set runtimepath=~/.vim,~/.vim/bundle/vundle,~/.vim/bundle/nerdtree,~/.vim/bundle/syntastic,~/.vim/bundle/Align,~/.vim/bundle/vcscommand.vim,~/.vim/bundle/vim-fugitive,~/.vim/bundle/vim-surround,~/.vim/bundle/vim-repeat,~/.vim/bundle/vim-colorschemes,~/.vim/bundle/vim-colors-solarized,~/.vim/bundle/vim-tomorrow-theme,~/.vim/bundle/desert-warm-256,~/.vim/bundle/ColorSchemeMenuMaker,~/.vim/bundle/vim-matchit,~/.vim/bundle/L9,~/.vim/bundle/FuzzyFinder,~/.vim/bundle/ack.vim,~/.vim/bundle/ctrlp.vim,~/.vim/bundle/unite.vim,~/.vim/bundle/DrawIt,~/.vim/bundle/vim-golang,~/.vim/bundle/vim-markdown,~/.vim/bundle/mru,~/.vim/bundle/vim-rsi,~/.vim/bundle/dbext.vim,/var/lib/vim/addons,/usr/share/vim/vimfiles,/usr/share/vim/vim74,/usr/share/vim/vimfiles/after,/var/lib/vim/addons/after,~/.vim/after,~/.vim/bundle/vundle/,~/.vim/bundle/vundle/after,~/.vim/bundle/nerdtree/after,~/.vim/bundle/syntastic/after,~/.vim/bundle/Align/after,~/.vim/bundle/vcscommand.vim/after,~/.vim/bundle/vim-fugitive/after,~/.vim/bundle/vim-surround/after,~/.vim/bundle/vim-repeat/after,~/.vim/bundle/vim-colorschemes/after,~/.vim/bundle/vim-colors-solarized/after,~/.vim/bundle/vim-tomorrow-theme/after,~/.vim/bundle/desert-warm-256/after,~/.vim/bundle/ColorSchemeMenuMaker/after,~/.vim/bundle/vim-matchit/after,~/.vim/bundle/L9/after,~/.vim/bundle/FuzzyFinder/after,~/.vim/bundle/ack.vim/after,~/.vim/bundle/ctrlp.vim/after,~/.vim/bundle/unite.vim/after,~/.vim/bundle/DrawIt/after,~/.vim/bundle/vim-golang/after,~/.vim/bundle/vim-markdown/after,~/.vim/bundle/mru/after,~/.vim/bundle/vim-rsi/after,~/.vim/bundle/dbext.vim/after
set shiftwidth=2
set showcmd
set smartcase
set softtabstop=2
set statusline=%<%{fugitive#statusline()}\ %f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
set suffixes=.bak,~,.swp,.o,.info,.aux,.log,.dvi,.bbl,.blg,.brf,.cb,.ind,.idx,.ilg,.inx,.out,.toc
set termencoding=utf-8
set title
set ttimeout
set ttimeoutlen=50
set visualbell
set window=39
" vim: set ft=vim :
